import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PokemonInfo, Pokemon } from "../models/pokemon.model";

@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    private _pokemon: Pokemon[] = [];
    private _error: string = '';

    constructor(private readonly http: HttpClient) {

    }

    fetchPokemons(): void {
        this.http.get<PokemonInfo>('https://pokeapi.co/api/v2/pokemon?limit=200')
        .subscribe(pokemonInfo => {
            this._pokemon = pokemonInfo.results
        }, (error: HttpErrorResponse) => {
            this._error = error.message
        })
    }

    public pokemon(): Pokemon[] {
        return this._pokemon;
    }

    public error(): string {
        return this._error;
    }
    

}