import { Injectable } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";

@Injectable({
    providedIn: 'root'
})
export class SelectedPokemonService {
    private _pokemon: Pokemon[] = [];
    private _selectedPokemonIndexes: number[] = [];

    public addPokemon(pokemon: Pokemon, i: number) {
        this._pokemon.push(pokemon);
        this._selectedPokemonIndexes.push(i)
    }

    public pokemon(): Pokemon[] {
        return this._pokemon;
    }

    public selectedPokemonIndexes(): number[] {
        return this._selectedPokemonIndexes;
    }
}