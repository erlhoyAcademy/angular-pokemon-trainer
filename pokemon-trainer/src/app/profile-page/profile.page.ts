import { Component } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { SelectedPokemonService } from "../services/selected-pokemon.service";

@Component({
    selector: 'app-profile-page',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.css']
})
export class ProfilePage {

    private _selectedPokemonIndexes: number[] = [];
    private _selectedPokemon: Pokemon[] = [];

    constructor(private readonly selectedPokemonService: SelectedPokemonService) {

        this._selectedPokemonIndexes = selectedPokemonService.selectedPokemonIndexes();
        this._selectedPokemon = selectedPokemonService.pokemon();
    }

    get selectedPokemon(): Pokemon[] {
        return this._selectedPokemon;
    }

    get selectedPokemonIndexes(): number[] {
        return this._selectedPokemonIndexes;
    }

    get localStoragePokemon(): string[] {
        let existing = localStorage.getItem('selectedPokemon');
        if (existing !== null) {
            return JSON.parse(existing)
        }
        else {
            return []
        }
    }

    get localStoragePokemonIndexes(): string[] {
        let existing = localStorage.getItem('selectedPokemonIndexes');
        if (existing !== null) {
            return JSON.parse(existing)
        }
        else {
            return []
        }
    }

}