import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginPage } from "./login-page/login.page";
import { PokemonListPage } from "./pokemon-list-page/pokemon-list.page";
import { ProfilePage } from "./profile-page/profile.page";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'pokemon',
        component: PokemonListPage
    },
    {
        path: 'profile',
        component: ProfilePage
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}