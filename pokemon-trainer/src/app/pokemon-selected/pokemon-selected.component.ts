import { Component } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { SelectedPokemonService } from "../services/selected-pokemon.service";

@Component({
   selector: 'app-pokemon-selected',
   templateUrl: './pokemon-selected.component.html',
   styleUrls: ['./pokemon-selected.component.css']
})
export class PokemonSelectedComponent {

   constructor(private readonly selectedPokemonService: SelectedPokemonService) {
      
   }

   get pokemon(): Pokemon[] | null {
      return this.selectedPokemonService.pokemon();
   }
}