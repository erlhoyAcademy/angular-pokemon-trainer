import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { Pokemon } from '../models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';
import { SelectedPokemonService } from '../services/selected-pokemon.service';

@Component({
    selector: 'app-pokemon-list-page',
    templateUrl: './pokemon-list.page.html',
    styleUrls: ['./pokemon-list.page.css']
})
export class PokemonListPage implements OnInit {

    private _alreadySelectedPokemon: string[] = [];
    private _alreadySelectedPokemonIndexes: number[] = [];

    constructor(
        private readonly pokemonService: PokemonService,
        private readonly selectedPokemonService: SelectedPokemonService,
        private router: Router
        ) 
        {
            
        }

    ngOnInit(): void {
        this.pokemonService.fetchPokemons();

        // redirect back to login page if the user doesn't have an active session
        if (localStorage.getItem('username') === null) {
            this.router.navigate(['/login'])
        }
    }

    get pokemon(): Pokemon[] {
        return this.pokemonService.pokemon();
    }

    get selectedPokemonIndexes(): number[] {
        return this.selectedPokemonService.selectedPokemonIndexes();
    }

    onPokemonClicked(pokemon: Pokemon, i: number): void {
        this.selectedPokemonService.addPokemon(pokemon, i);
    }
    
    // Triggers when the user clicks the button to go to the collection (trainer page)
    onGoForwardClick(): void {

        // existing1 and 2 refers to already existing localStorage
        const existing1 = localStorage.getItem('selectedPokemon')
        const existing2 = localStorage.getItem('selectedPokemonIndexes')

        // If there is already selected some pokemon
        if (existing1 !== null && existing2 !== null) {

            localStorage.setItem('selectedPokemon', JSON.stringify(
                JSON.parse(existing1).concat(this.selectedPokemonService.pokemon().map(p => p.name))
            ));
            

            localStorage.setItem('selectedPokemonIndexes', JSON.stringify(
                [...JSON.parse(existing2), ...this.selectedPokemonService.selectedPokemonIndexes()]
            ));
                
        }
        else {
            localStorage.setItem('selectedPokemon', JSON.stringify(this.selectedPokemonService.pokemon().map(p => p.name)))
            localStorage.setItem('selectedPokemonIndexes', JSON.stringify(this.selectedPokemonService.selectedPokemonIndexes()))
        }
    }

}