import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PokemonListPage } from './pokemon-list-page/pokemon-list.page';
import { LoginPage} from './login-page/login.page';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PokemonSelectedComponent } from './pokemon-selected/pokemon-selected.component';
import { AppRoutingModule } from './app-routing.module';
import { ProfilePage } from './profile-page/profile.page';

@NgModule({
  declarations: [
    AppComponent,
    PokemonListPage,
    LoginPage,
    PokemonSelectedComponent,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
