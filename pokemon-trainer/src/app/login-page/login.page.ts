import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from '@angular/router'
import { NgbPagination } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-login-page',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})

export class LoginPage implements OnInit {

    ngOnInit(): void {
        // redirect to catalogue if the user already has an active session
        if (localStorage.getItem('username') !== null) {
            this.router.navigate(['/pokemon'])
        }
    }

    private _username: string = ''

    constructor(private router: Router) {

    }

    private setUsername(username: string): void {
        this._username = username;
    }
    
    public onLoginClicked(formData: NgForm): void {
        
        if (formData.valid) {
            this._username = formData.value.username;
            localStorage.setItem('username', this._username);
            this.router.navigate(['/pokemon']);
            localStorage.setItem('pokeIndex', '1');
        }
    }


}